## Shells and Slides

Four tracks made with [TidalCycles](https://tidalcycles.org), each created by manipulating just one audio sample—rolling, sliding through it, mostly with patterns for the `begin` function.

Digital album
↷ [nervousdata.bandcamp.com/album/shells-and-slides](https://nervousdata.bandcamp.com/album/shells-and-slides)

1. But You Keep Me Hanging On (01:10)
2. I’m Leaving You Tomorrow (03:38)
3. You Can Have It (03:05)
4. And Suddenly My Heart Goes Boom (03:21)
